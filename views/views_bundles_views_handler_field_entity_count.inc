<?php
/**
 * @file
 * Definition of views_bundles_views_handler_field_entity_count.
 */

/**
 * Field handler to provide simple entity count for the current bundle type.
 *
 * @ingroup views_field_handlers
 */
class views_bundles_views_handler_field_entity_count extends views_handler_field {
  /**
   * @param $values
   *
   * @return string
   */
  function render($values) {
    $query = new EntityFieldQuery();
    $query->entityCondition('entity_type', $values->entity_type)
      ->entityCondition('bundle', $values->bundle_type)
      ->addMetaData('account', user_load(1));
    $count = $query->count()->execute();

    return $count;
  }
}
