<?php
/**
 * @file
 * Definition of views_bundles_views_handler_field_manage_fields_path.
 */

/**
 * Field handler to provide simple renderer that turns a URL into a clickable link.
 *
 * @ingroup views_field_handlers
 */
class views_bundles_views_handler_field_manage_fields_path extends views_handler_field {
  /**
   * @param $values
   *
   * @return string
   */
  function render($values) {
    $info = views_bundles_get_info();
    if (isset($info[$values->entity_type]['manage fields path'])) {
      $bundle = str_replace('_', '-', $values->bundle_type);
      $value = str_replace('%bundle', $bundle, $info[$values->entity_type]['manage fields path']);
      if (!drupal_valid_path($value)) {
        return FALSE;
      }

      $this->options['alter']['make_link'] = TRUE;
      $this->options['alter']['path'] = $value;
      $text = !empty($this->options['text']) ? $this->sanitize_value($this->options['text']) : $this->sanitize_value($value, 'url');
      return $text;
    }
    return FALSE;
  }
}
