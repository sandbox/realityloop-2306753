<?php

/**
 * Class views_bundles_views_plugin_query
 */
class views_bundles_views_plugin_query extends views_plugin_query {

  /**
   * Add a field to the query table, possibly with an alias.
   *
   * @param $table
   * @param $field
   * @param $alias
   * @param $params
   *
   * @return $name
   *   The name that this field can be referred to as. Usually this is the alias.
   */
  function add_field($table, $field, $alias = '', $params = array()) {
    return $field;
  }

  /**
   * Executes the query and fills the associated view object with according
   * values.
   *
   * Values to set: $view->result, $view->total_rows, $view->execute_time,
   * $view->pager['current_page'].
   *
   * $view->result should contain an array of objects. The array must use a
   * numeric index starting at 0.
   *
   * @param view $view
   *   The view which is executed.
   */
  function execute(&$view) {
    $entity_types = entity_get_info();
    $entity       = NULL;
    if ($view->base_table != 'views_bundles__base') {
      $entity = substr($view->base_table, 14);
    }

    foreach ($entity_types as $entity_type => $entity_info) {
      if ((!is_null($entity) && $entity_type != $entity) || !views_bundles_is_supported($entity_type)) {
        continue;
      }

      foreach ($entity_info['bundles'] as $bundle_type => $bundle) {
        $result = array(
          'entity_type'        => $entity_type,
          'entity_name'        => $entity_info['label'],
          // The Bundle type filter requires the Entity type to be included in
          // the case that two entities share a bundle name. This is undone
          // prior to the result being set.
          'bundle_type'        => "{$entity_type}::{$bundle_type}",
          'bundle_name'        => $bundle['label'],
          'bundle_description' => isset($bundle['description']) ? $bundle['description'] : '',
        );

        // Apply filters.
        if (!empty($this->where)) {
          foreach ($this->where as $where) {
            $conditions = array();

            // Build result of conditons.
            foreach ($where['conditions'] as $condition) {
              $field = substr($condition['field'], 1);
              switch ($condition['operator']) {
                // @TODO - Add the other operators.
                case 'in':
                  $conditions[] = in_array($result[$field], $condition['value']);
                  break;

                case 'not in':
                  $conditions[] = !in_array($result[$field], $condition['value']);
                  break;
              }
            }

            $filter = $where['type'] == 'AND' ? in_array(FALSE, $conditions) : !in_array(TRUE, $conditions);
          }
        }

        // Add item to results if it's not to be filtered out.
        if (!$filter) {
          $result['bundle_type'] = $bundle_type;
          $view->result[]        = (object) $result;
        }
      }
    }
  }

  /**
   * Add settings for the ui.
   */
  function options_form(&$form, &$form_state) {
    parent::options_form($form, $form_state);
  }

  /**
   * Generate a query and a countquery from all of the information supplied
   * to the object.
   *
   * @param $get_count
   *   Provide a countquery if this is true, otherwise provide a normal query.
   */
  function query($get_count = FALSE) {
  }

  function add_where($group, $field, $value = NULL, $operator = NULL) {
    // Ensure all variants of 0 are actually 0. Thus '', 0 and NULL are all
    // the default group.
    if (empty($group)) {
      $group = 0;
    }

    // Check for a group.
    if (!isset($this->where[$group])) {
      $this->set_where_group('AND', $group);
    }

    $this->where[$group]['conditions'][] = array(
      'field'    => $field,
      'value'    => $value,
      'operator' => $operator,
    );
  }

}
