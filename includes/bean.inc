<?php
/**
 * @file
 * Bean module integration.
 */

/**
 * Implements hook_views_bundles_info() on behalf of bean.module.
 */
function bean_views_bundles_info() {
  return array(
    'bean' => array(
      'create path' => 'block/add/%bundle',
      'edit path' => 'admin/structure/block-types/manage/%bundle/edit',
      'manage fields path' => 'admin/structure/block-types/manage/%bundle/fields',
      'manage display path' => 'admin/structure/block-types/manage/%bundle/display',
    ),
  );
}
