<?php
/**
 * @file
 * Entityforms module integration.
 */

/**
 * Implements hook_views_bundles_info() on behalf of entityform.module.
 */
function entityform_views_bundles_info() {
  return array(
    'entityform' => array(
      'create path' => 'eform/submit/%bundle',
      'edit path' => 'admin/structure/entityform_types/manage/%bundle',
      'manage fields path' => 'admin/structure/entityform_types/manage/%bundle/fields',
      'manage display path' => 'admin/structure/entityform_types/manage/%bundle/display',
    ),
    'entityform_type' => array(
      'create path' => 'admin/structure/entityform_types/add',
    ),
  );
}
