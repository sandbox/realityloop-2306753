<?php
/**
 * @file
 * User module integration.
 */

/**
 * Implements hook_views_bundles_info() on behalf of user.module.
 */
function user_views_bundles_info() {
  return array(
    'user' => array(
      'create path' => 'admin/people/create',
      'edit path' => 'admin/config/people/accounts',
      'manage fields path' => 'admin/config/people/accounts/fields',
      'manage display path' => 'admin/config/people/accounts/display',
    ),
  );
}
