<?php
/**
 * @file
 * Rules module integration.
 */

/**
 * Implements hook_views_bundles_info() on behalf of rules.module.
 */
function rules_views_bundles_info() {
  return array(
    'rules_config' => array(
      'create path' => 'admin/config/workflow/rules/reaction/add',
    ),
  );
}
