<?php
/**
 * @file
 * Taxonomy module integration.
 */

/**
 * Implements hook_views_bundles_info() on behalf of taxonomy.module.
 */
function taxonomy_views_bundles_info() {
  return array(
    'taxonomy_term' => array(
      'create path' => 'admin/structure/taxonomy/%bundle/add',
    ),
    'taxonomy_vocabulary' => array(
      'create path' => 'admin/structure/taxonomy/add',
    ),
  );
}
