<?php
/**
 * @file
 * Node module integration.
 */

/**
 * Implements hook_views_bundles_info() on behalf of node.module.
 */
function node_views_bundles_info() {
  return array(
    'node' => array(
      'create path' => 'node/add/%bundle',
      'edit path' => 'admin/structure/types/manage/%bundle',
      'manage fields path' => 'admin/structure/types/manage/%bundle/fields',
      'manage display path' => 'admin/structure/types/manage/%bundle/display',
    ),
  );
}
