<?php
/**
 * @file
 * File entity module integration.
 */

/**
 * Implements hook_views_bundles_info() on behalf of file_entity.module.
 */
function file_entity_views_bundles_info() {
  return array(
    'file' => array(
      'create path' => 'file/add',
      'edit path' => 'admin/structure/file-types/manage/%bundle/edit',
      'manage fields path' => 'admin/structure/file-types/manage/%bundle/fields',
      'manage display path' => 'admin/structure/file-types/manage/%bundle/display',
    ),
  );
}
