<?php
/**
 * @file
 * Multifield module integration.
 */

/**
 * Implements hook_views_bundles_supported_alter() on behalf of
 * multifield.module.
 */
function multifield_views_bundles_supported_alter(&$supported) {
  unset($supported['multifield']);
}
